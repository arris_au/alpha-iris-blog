<?php

namespace AlphaIris\Blog\Providers;

use Illuminate\Support\Facades\App;
use Illuminate\Support\ServiceProvider;

class BlogServiceProvider extends ServiceProvider
{
    public function boot()
    {
        $this->loadMigrationsFrom(__DIR__.'/../../database/migrations');

        $this->loadRoutesFrom(__DIR__.'../../../routes/web.php');

    }

    public function register()
    {
        $this->loadViewsFrom([
            App::basePath().'/resources/views/alpha-iris-blog',
            __DIR__.'/../../resources/views/',
            __DIR__.'/../../resources/views/vendor/alpha-iris',
        ], 'alpha-iris-blog');
    }
}
